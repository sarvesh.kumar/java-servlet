package com.wipro.servlets.internationalization;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Assignment1")
public class Assignment1 extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException

	{
		PrintWriter out = response.getWriter();
		out.println("Welcome to First Assignment to display Hello to Spanish speakers, with the localized time");
		out.println();
		
		Locale locale;
		DateFormat full;
		try {
			locale = new Locale("es", "");
			full = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			out.println("In Spanish :");
			out.println("\u00a1Hola Mundo!");
			out.println(full.format(new Date()));
			out.println();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
