package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servlet.Dao.LoginDao;

/**
 * Servlet implementation class AuthServlet
 */
@WebServlet("/Login")
public class AuthServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LoginDao dao=new LoginDao();
		String uname=request.getParameter("name");
		String pass=request.getParameter("pass");
		PrintWriter out=response.getWriter();
		
		if(dao.checkDetails(uname, pass))
		{
			out.print("Name/Password Match");
		}
		else
		{
			out.print("Name/Password Does Not Match");
		}
		
	}

	

}
