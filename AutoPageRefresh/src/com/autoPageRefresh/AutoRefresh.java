package com.autoPageRefresh;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AutoRefresh")
public class AutoRefresh extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setIntHeader("refresh", 5);
		Date thisDate=new Date();
		SimpleDateFormat dateForm = new SimpleDateFormat("yyyy.MM.dd  hh:mm:ss ");
		String myString = dateForm.format(thisDate);
		PrintWriter out=response.getWriter();
		out.print("<html><body><h1>Auto Page Refresh Page that will refresh in 5 second</h1></body></html>");
		out.print(myString);
		
	}

	
}
